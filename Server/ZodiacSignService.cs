﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Server
{
    internal class ZodiacSignService : Generated.ZodiacSignService.ZodiacSignServiceBase
    {
        public override Task<ZodiacSignResponse> GetZodiacSign(ZodiacSignRequest request, ServerCallContext context)
        {
            System.Console.WriteLine(request.Date, "\n");
            var result = new ZodiacSignResponse();
            result.Sign = GetSign(request.Date);
            return Task.FromResult(result);
        }
        private List<DateTime> ReadFromFile()
        {
            string[] lines = System.IO.File.ReadAllLines("..//ZodiacSigns.txt");
            List<DateTime> list = new List<DateTime>();
            foreach (string s in lines)
                list.Add(DateTime.ParseExact(s, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None));
            return list;
        }

        private string GetSign(string date)
        {
            List<DateTime> list = ReadFromFile();
            DateTime dt = DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
            int zodiacIndex = 0;
            for (int index = 0; index < list.Count; index++)
            {
                if (dt.Month == list[index].Month)
                {
                    if (dt.Day <= list[index].Day)
                        zodiacIndex = index;
                    else
                        zodiacIndex = ++index;
                    index = list.Count;
                }
            }
            switch (zodiacIndex)
            {
                case 0:
                    return "Capricorn";
                case 1:
                    return "Aquarius";
                case 2:
                    return "Pisces";
                case 3:
                    return "Aries";
                case 4:
                    return "Taurus";
                case 5:
                    return "Gemini";
                case 6:
                    return "Cancer";
                case 7:
                    return "Leo";
                case 8:
                    return "Virgo";
                case 9:
                    return "Libra";
                case 10:
                    return "Scorpio";
                case 11:
                    return "Sagittarius";
                case 12:
                    return "Capricorn";
                default:
                    return "Unknown sign";
            }
        }
    }
}
