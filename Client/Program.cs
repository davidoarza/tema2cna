﻿using Grpc.Core;
using System;
using System.Globalization;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Host = "localhost";
            const int Port = 16842;

            var channel = new Channel($"{Host}:{Port}", ChannelCredentials.Insecure);

            Console.WriteLine("Insert date: ");

            string key = Console.ReadLine();
            try
            {
                DateTime.ParseExact(key, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                var client = new Generated.ZodiacSignService.ZodiacSignServiceClient(channel);
                var response = client.GetZodiacSign(new Generated.ZodiacSignRequest() { Date = key });
                Console.WriteLine(response);
            }
            catch (Exception e)
            {
                Console.WriteLine("\nDate not valid");
            }

            // Shutdown
            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
